import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import {
    FeedbackActionTypes, DialogActionTypes, VersionActionTypes,
    SkillsActionTypes, AccountsActionTypes, enqueueFeedback, dequeueFeedback,
    fetchVersion, fetchSkills, createSkill, deleteSkill, fetchAccounts,
    createAccount, updateAccount, deleteAccount, addAccount, editAccount,
    closeDialog,
} from './actions';
import { DialogTypes } from './containers/DialogWindow';

const mockStore = configureStore([thunk]);

function mockFetch(status, json) {
    const res = new window.Response(JSON.stringify(json), {
        status,
        headers: { 'Content-Type': 'application/json' },
    });

    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(res));
}

describe('actions', () => {
    test('can create feedback actions', () => {
        const message = 'Test message';

        expect(enqueueFeedback(message)).toEqual({
            type: FeedbackActionTypes.ENQUEUE,
            message,
        });
        expect(dequeueFeedback()).toEqual({
            type: FeedbackActionTypes.DEQUEUE,
        });
    });

    test('can create dialog actions', () => {
        const account = { username: 'test' };

        expect(addAccount()).toEqual({
            type: DialogActionTypes.OPEN,
            dialogType: DialogTypes.ADD_ACCOUNT,
            dialogProps: {},
        });
        expect(editAccount(account)).toEqual({
            type: DialogActionTypes.OPEN,
            dialogType: DialogTypes.EDIT_ACCOUNT,
            dialogProps: { account },
        });
        expect(closeDialog()).toEqual({ type: DialogActionTypes.CLOSE });
    });

    test('can fetch version', async () => {
        const store = mockStore({ version: { api: '' } });

        mockFetch(200, { api: '0.1' });
        await store.dispatch(fetchVersion());
        expect(store.getActions()).toEqual([{
            type: VersionActionTypes.REQUEST,
        }, {
            type: VersionActionTypes.SUCCESS,
            version: { api: '0.1' },
        }]);
    });

    test('version fetch error is handled', async () => {
        const store = mockStore({ version: { api: '' } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchVersion());
        expect(store.getActions()).toEqual([{
            type: VersionActionTypes.REQUEST,
        }, {
            type: VersionActionTypes.FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch version', () => {
        window.fetch = jest.fn();

        let store = mockStore({ version: { api: '0.1' } });
        store.dispatch(fetchVersion());
        expect(window.fetch).not.toBeCalled();

        store = mockStore({ version: { isFetching: true } });
        store.dispatch(fetchVersion());
        expect(window.fetch).not.toBeCalled();
    });

    test('can fetch skills', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(200, { resources: [{ name: 'A' }, { name: 'B' }] });
        await store.dispatch(fetchSkills());
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.FETCH_REQUEST,
        }, {
            type: SkillsActionTypes.FETCH_SUCCESS,
            skills: [{ name: 'A' }, { name: 'B' }],
        }]);
    });

    test('skill fetch error is handled', async () => {
        const store = mockStore({ skills: { isFetching: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchSkills());
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.FETCH_REQUEST,
        }, {
            type: SkillsActionTypes.FETCH_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch skills', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isFetching: true } });
        store.dispatch(fetchSkills());
        expect(window.fetch).not.toBeCalled();
    });

    test('can create a skill', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(201, { name: 'A' });
        await store.dispatch(createSkill('A'));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.CREATE_REQUEST,
        }, {
            type: SkillsActionTypes.CREATE_SUCCESS,
            skill: { name: 'A' },
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A skill created',
        }]);
    });

    test('skill create error is handled', async () => {
        const store = mockStore({ skills: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(createSkill('A'));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.CREATE_REQUEST,
        }, {
            type: SkillsActionTypes.CREATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-create a skill', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isCreating: true } });
        store.dispatch(createSkill('A'));
        expect(window.fetch).not.toBeCalled();
    });

    test('can delete a skill', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(200, {});
        await store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.DELETE_REQUEST,
        }, {
            type: SkillsActionTypes.DELETE_SUCCESS,
            id: '1',
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A skill deleted',
        }]);
    });

    test('skill delete error is handled', async () => {
        const store = mockStore({ skills: { isDeleting: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.DELETE_REQUEST,
        }, {
            type: SkillsActionTypes.DELETE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-delete a skill', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isDeleting: true } });
        store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can fetch accounts', async () => {
        const store = mockStore({ accounts: {} });

        mockFetch(200, { resources: [{ username: 'A' }, { username: 'B' }] });
        await store.dispatch(fetchAccounts());
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.FETCH_REQUEST,
        }, {
            type: AccountsActionTypes.FETCH_SUCCESS,
            accounts: [{ username: 'A' }, { username: 'B' }],
        }]);
    });

    test('account fetch error is handled', async () => {
        const store = mockStore({ accounts: { isFetching: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchAccounts());
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.FETCH_REQUEST,
        }, {
            type: AccountsActionTypes.FETCH_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch accounts', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isFetching: true } });
        store.dispatch(fetchAccounts());
        expect(window.fetch).not.toBeCalled();
    });

    test('can create a account', async () => {
        const store = mockStore({ accounts: {} });

        mockFetch(201, { username: 'A' });
        await store.dispatch(createAccount({ username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.CREATE_REQUEST,
        }, {
            type: AccountsActionTypes.CREATE_SUCCESS,
            account: { username: 'A' },
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A\'s account created',
        }]);
    });

    test('account create error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(createAccount({ username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.CREATE_REQUEST,
        }, {
            type: AccountsActionTypes.CREATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-create a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isCreating: true } });
        store.dispatch(createAccount({ username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can update a account', async () => {
        const store = mockStore({
            accounts: { items: [{ id: '1', username: 'A' }] },
        });

        mockFetch(200, { id: '1', username: 'A' });
        await store.dispatch(updateAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.UPDATE_REQUEST,
        }, {
            type: AccountsActionTypes.UPDATE_SUCCESS,
            account: { id: '1', username: 'A' },
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A\'s account updated',
        }]);
    });

    test('account update error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(updateAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.UPDATE_REQUEST,
        }, {
            type: AccountsActionTypes.UPDATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-update a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isUpdating: true } });
        store.dispatch(updateAccount({ id: '1', username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can delete a account', async () => {
        const store = mockStore({ accounts: {} });

        mockFetch(200, {});
        await store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.DELETE_REQUEST,
        }, {
            type: AccountsActionTypes.DELETE_SUCCESS,
            id: '1',
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A\'s account deleted',
        }]);
    });

    test('account delete error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.DELETE_REQUEST,
        }, {
            type: AccountsActionTypes.DELETE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-delete a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isDeleting: true } });
        store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });
});
