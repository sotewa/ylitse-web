import React from 'react';
import { shallow, mount } from 'enzyme';
import ChipSelect from './ChipSelect';

describe('ChipSelect', () => {
    const onRemoveItem = jest.fn();

    const availableItems = [
        { name: 'F6BAB4966X', id: 'f6bab4966x' },
        { name: 'ZHTLBN7GZ8', id: 'zhtlbn7gz8' },
        { name: '7K5J4I4J7D', id: '7k5j4i4j7d' },
        { name: 'P7E124IALI', id: 'p7e124iali' },
        { name: 'F4AWNF4COZ', id: 'f4awnf4coz' },
        { name: 'NMT10X4E6H', id: 'nmt10x4e6h' },
        { name: '0CW2WL81FX', id: '0cw2wl81fx' },
        { name: 'GLP2GZ2UVF', id: 'glp2gz2uvf' },
    ];

    const props = {
        availableItems,
        onRemoveItem,
        selectedItem: [],
        itemName: 'skill',
    };

    test('renders correctly', () => {
        const wrapper = shallow(<ChipSelect {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('Input receive correct props', () => {
        const wrapper = mount(<ChipSelect
            {...props}
            inputValue="a test value"
        />);

        expect(wrapper.props().inputValue).toEqual('a test value');
        expect(wrapper.find('TextField').props().InputProps.value)
            .toEqual('a test value');
        expect(wrapper.find('input').props().value).toEqual('a test value');
    });

    test('Input change value correctly', () => {
        const wrapper = mount(<ChipSelect {...props} />);

        wrapper.setProps({ inputValue: 'second value' });

        expect(wrapper.props().inputValue).toEqual('second value');
        expect(wrapper.find('TextField').props().InputProps.value)
            .toEqual('second value');
        expect(wrapper.find('input').props().value).toEqual('second value');
    });

    test('render chips correctly', () => {
        const selectedItem = ['F6BAB4966X'];
        const wrapper = mount(<ChipSelect
            {...props}
            selectedItem={selectedItem}
        />);
        expect(wrapper.find('Chip').props().label)
            .toEqual(selectedItem.join(''));
        expect(wrapper.find('Downshift').props().selectedItem)
            .toEqual(selectedItem);
    });

    test('get the correct suggestion', () => {
        const wrapper = mount(<ChipSelect
            {...props}
            inputValue="f4"
            isOpen
        />);

        expect(wrapper.find('MenuItem').children().props().children)
            .toEqual('F4AWNF4COZ');
    });

    test('open list on click', () => {
        const wrapper = mount(<ChipSelect {...props} />);
        expect(wrapper.find('MenuItem').children().length).toEqual(0);
        wrapper.find('input').simulate('click');
        expect(wrapper.find('MenuItem').children().length).toEqual(8);
    });
});
