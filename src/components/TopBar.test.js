import React from 'react';
import { shallow } from 'enzyme';

import TopBar from './TopBar';

describe('TopBar', () => {
    test('renders correctly', () => {
        const props = {
            username: 'test',
            toolbarLinks: [{ label: 'Test', component: 'button' }],
            version: { ui: '0.1', api: '0.1' },
            onLogoutClick: () => {},
        };

        const wrapper = shallow(<TopBar {...props} />).dive();
        expect(wrapper).toMatchSnapshot();
    });

    test('callbacks work', () => {
        const props = {
            username: '',
            toolbarLinks: [],
            version: { ui: '', api: '' },
            onLogoutClick: () => {},
        };
        const wrapper = shallow(<TopBar {...props} />).dive();

        const aboutButton = wrapper.find('[name="about"]');
        aboutButton.simulate('click');
        expect(wrapper.state('aboutOpen')).toBeTruthy();

        const menuButton = wrapper.find('[name="menu"]');
        menuButton.simulate('click', {
            currentTarget: menuButton,
        });
        expect(wrapper.state('menuAnchor')).not.toBeNull();
    });
});
