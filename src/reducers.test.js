import { feedback, dialog, version, skills, accounts } from './reducers';
import {
    FeedbackActionTypes, DialogActionTypes, VersionActionTypes,
    SkillsActionTypes, AccountsActionTypes,
} from './actions';

describe('feedback', () => {
    test('returns initial state', () => {
        expect(feedback(undefined, {})).toEqual({ messages: [], current: '' });
    });

    test('enqueues a message', () => {
        const state = { messages: ['Test 1', 'Test 2'], current: 'Test 0' };
        const action = { type: FeedbackActionTypes.ENQUEUE, message: 'Test 3' };

        expect(feedback(state, action)).toEqual({
            messages: ['Test 1', 'Test 2', 'Test 3'],
            current: 'Test 0',
        });
    });

    test('returns the only message right away', () => {
        const state = { current: '' };
        const action = { type: FeedbackActionTypes.ENQUEUE, message: 'Test' };

        expect(feedback(state, action)).toEqual({
            messages: [],
            current: 'Test',
        });
    });

    test('doesn\'t enqueue the same message as current', () => {
        const state = { messages: ['Test 1', 'Test 2'], current: 'Test 0' };
        const action = { type: FeedbackActionTypes.ENQUEUE, message: 'Test 0' };

        expect(feedback(state, action)).toEqual(state);
    });

    test('dequeues a message', () => {
        const state = { messages: ['Test 1', 'Test 2'], current: 'Test 0' };
        const action = { type: FeedbackActionTypes.DEQUEUE };

        expect(feedback(state, action)).toEqual({
            messages: ['Test 2'],
            current: 'Test 1',
        });
        expect(feedback({ messages: [], current: 'Test 0' }, action)).toEqual({
            messages: [],
            current: '',
        });
    });
});

describe('dialog', () => {
    test('returns initial state', () => {
        expect(dialog(undefined, {})).toEqual({ type: '', props: {} });
    });

    test('can be opened', () => {
        const state = { type: '', props: [] };
        const action = {
            type: DialogActionTypes.OPEN,
            dialogType: 'TEST',
            dialogProps: { prop: '' },
        };

        expect(dialog(state, action)).toEqual({
            type: 'TEST',
            props: { prop: '' },
        });
    });

    test('can be closed', () => {
        const action = { type: DialogActionTypes.CLOSE };

        expect(dialog({}, action)).toEqual({ type: '', props: {} });
    });
});

describe('version', () => {
    test('returns initial state', () => {
        expect(version(undefined, {})).toEqual({ api: '', isFetching: false });
    });

    test('starts fetching version', () => {
        const state = { api: '', isFetching: true };
        const action = { type: VersionActionTypes.REQUEST };

        expect(version(state, action)).toEqual({ api: '', isFetching: true });
    });

    test('returns fetched version', () => {
        const state = { api: '', isFetching: true };
        const action = {
            type: VersionActionTypes.SUCCESS,
            version: { api: '0.1' },
        };

        expect(version(state, action)).toEqual({
            api: '0.1',
            isFetching: false,
        });
    });

    test('fetching failed', () => {
        const state = { api: '', isFetching: true };
        const action = { type: VersionActionTypes.FAILURE };

        expect(version(state, action)).toEqual({ api: '', isFetching: false });
    });
});

describe('skills', () => {
    const initialState = {
        items: [],
        isFetching: false,
        isCreating: false,
        isDeleting: false,
    };

    test('returns initial state', () => {
        expect(skills(undefined, {})).toEqual(initialState);
    });

    test('starts fetching skills', () => {
        const action = { type: SkillsActionTypes.FETCH_REQUEST };

        expect(skills(initialState, action)).toEqual({
            ...initialState,
            isFetching: true,
        });
    });

    test('returns fetched and sorted skills', () => {
        const state = { ...initialState, isFetching: true };
        const action = {
            type: SkillsActionTypes.FETCH_SUCCESS,
            skills: [{ name: 'c' }, { name: 'a' }, { name: 'B' }],
        };

        expect(skills(state, action)).toEqual({
            ...initialState,
            items: [{ name: 'a' }, { name: 'B' }, { name: 'c' }],
            isFetching: false,
        });
    });

    test('fetching failed', () => {
        const state = { ...initialState, isFetching: true };
        const action = { type: SkillsActionTypes.FETCH_FAILURE };

        expect(skills(state, action)).toEqual({
            ...initialState,
            isFetching: false,
        });
    });

    test('starts creating a skill', () => {
        const action = { type: SkillsActionTypes.CREATE_REQUEST };

        expect(skills(initialState, action)).toEqual({
            ...initialState,
            isCreating: true,
        });
    });

    test('returns skills with a newly added one', () => {
        const state = {
            ...initialState,
            items: [{ name: 'a' }, { name: 'c' }],
            isCreating: true,
        };
        const action = {
            type: SkillsActionTypes.CREATE_SUCCESS,
            skill: { name: 'B' },
        };

        expect(skills(state, action)).toEqual({
            ...initialState,
            items: [{ name: 'a' }, { name: 'B' }, { name: 'c' }],
            isCreating: false,
        });
    });

    test('creating failed', () => {
        const state = { ...initialState, isCreating: true };
        const action = { type: SkillsActionTypes.CREATE_FAILURE };

        expect(skills(state, action)).toEqual({
            ...initialState,
            isCreating: false,
        });
    });

    test('starts deleting a skill', () => {
        const action = { type: SkillsActionTypes.DELETE_REQUEST };

        expect(skills(initialState, action)).toEqual({
            ...initialState,
            isDeleting: true,
        });
    });

    test('returns skills w/o a deleted one', () => {
        const state = {
            ...initialState,
            items: [{ id: '0', name: 'a' }, { id: '1', name: 'B' }],
            isDeleting: true,
        };
        const action = {
            type: SkillsActionTypes.DELETE_SUCCESS,
            id: '1',
        };

        expect(skills(state, action)).toEqual({
            ...initialState,
            items: [{ id: '0', name: 'a' }],
            isDeleting: false,
        });
    });

    test('deleting failed', () => {
        const state = { ...initialState, isDeleting: true };
        const action = { type: SkillsActionTypes.DELETE_FAILURE };

        expect(skills(state, action)).toEqual({
            ...initialState,
            isDeleting: false,
        });
    });
});

describe('accounts', () => {
    const initialState = {
        items: [],
        isFetching: false,
        isCreating: false,
        isUpdating: false,
        isDeleting: false,
    };

    test('returns initial state', () => {
        expect(accounts(undefined, {})).toEqual(initialState);
    });

    test('starts fetching accounts', () => {
        const action = { type: AccountsActionTypes.FETCH_REQUEST };

        expect(accounts(initialState, action)).toEqual({
            ...initialState,
            isFetching: true,
        });
    });

    test('returns fetched and sorted accounts', () => {
        const state = { ...initialState, isFetching: true };
        const action = {
            type: AccountsActionTypes.FETCH_SUCCESS,
            accounts: [{ username: 'c' }, { username: 'a' }, { username: 'B' }],
        };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            items: [{ username: 'a' }, { username: 'B' }, { username: 'c' }],
            isFetching: false,
        });
    });

    test('fetching failed', () => {
        const state = { ...initialState, isFetching: true };
        const action = { type: AccountsActionTypes.FETCH_FAILURE };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            isFetching: false,
        });
    });

    test('starts creating a account', () => {
        const action = { type: AccountsActionTypes.CREATE_REQUEST };

        expect(accounts(initialState, action)).toEqual({
            ...initialState,
            isCreating: true,
        });
    });

    test('returns accounts with a newly added one', () => {
        const state = {
            ...initialState,
            items: [{ username: 'a' }, { username: 'c' }],
            isCreating: true,
        };
        const action = {
            type: AccountsActionTypes.CREATE_SUCCESS,
            account: { username: 'B' },
        };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            items: [{ username: 'a' }, { username: 'B' }, { username: 'c' }],
            isCreating: false,
        });
    });

    test('creating failed', () => {
        const state = { ...initialState, isCreating: true };
        const action = { type: AccountsActionTypes.CREATE_FAILURE };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            isCreating: false,
        });
    });

    test('starts updating an account', () => {
        const action = { type: AccountsActionTypes.UPDATE_REQUEST };

        expect(accounts(initialState, action)).toEqual({
            ...initialState,
            isUpdating: true,
        });
    });

    test('returns updated and sorted accounts', () => {
        const state = {
            ...initialState,
            items: [{ id: '0', username: 'a' }, { id: '1', username: 'b' }],
            isUpdating: true,
        };
        const action = {
            type: AccountsActionTypes.UPDATE_SUCCESS,
            account: { id: '1', username: 'B' },
        };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            items: [{ id: '0', username: 'a' }, { id: '1', username: 'B' }],
            isUpdating: false,
        });
    });

    test('updating failed', () => {
        const state = { ...initialState, isUpdating: true };
        const action = { type: AccountsActionTypes.UPDATE_FAILURE };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            isUpdating: false,
        });
    });

    test('starts deleting a account', () => {
        const action = { type: AccountsActionTypes.DELETE_REQUEST };

        expect(accounts(initialState, action)).toEqual({
            ...initialState,
            isDeleting: true,
        });
    });

    test('returns accounts w/o a deleted one', () => {
        const state = {
            ...initialState,
            items: [{ id: '0', username: 'a' }, { id: '1', username: 'B' }],
            isDeleting: true,
        };
        const action = {
            type: AccountsActionTypes.DELETE_SUCCESS,
            id: '1',
        };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            items: [{ id: '0', username: 'a' }],
            isDeleting: false,
        });
    });

    test('deleting failed', () => {
        const state = { ...initialState, isDeleting: true };
        const action = { type: AccountsActionTypes.DELETE_FAILURE };

        expect(accounts(state, action)).toEqual({
            ...initialState,
            isDeleting: false,
        });
    });
});
