import { configure } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import 'whatwg-fetch';

configure({ adapter: new EnzymeAdapter() });

// Throw prop type errors in tests
// eslint-disable-next-line no-console
console.error = (message) => {
    throw new Error(message);
};
