import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions, DialogContent, DialogTitle, withMobileDialog,
} from 'material-ui/Dialog';
import Tabs, { Tab } from 'material-ui/Tabs';

import { UserRoles, UserGenders, Languages } from '../constants';
import {
    fetchSkills, createAccount, updateAccount, closeDialog,
} from '../actions';
import Loading from '../components/Loading';
import AccountForm from '../components/AccountForm';
import MentorForm from '../components/MentorForm';

// TODO: Make a list when replaced with translations
export const AvailableChannels = {
    phone: 'Phone',
    email: 'Email',
    chat: 'Chat',
};

const emptyState = {
    selectedTab: 0,
    account: {
        role: UserRoles.MENTOR,
        username: '',
        password: '',
        nickname: '',
        email: '',
        phone: '',
        gender: UserGenders.MALE,
        birthYear: '',
        area: '',
        languages: [],
        skills: [],
        communicationChannels: [],
        story: '',
    },
    errors: {
        username: undefined,
        password: undefined,
    },
    formValid: false,
    inputSkills: '',
    inputLanguages: '',
};

export class AccountDialog extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        fullScreen: PropTypes.bool.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        account: PropTypes.object,
        availableSkills: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })).isRequired,
        isFetchingSkills: PropTypes.bool.isRequired,
        fetchSkills: PropTypes.func.isRequired,
        createAccount: PropTypes.func.isRequired,
        updateAccount: PropTypes.func.isRequired,
        closeDialog: PropTypes.func.isRequired,
    }

    static defaultProps = {
        account: undefined,
    }

    constructor(props) {
        super(props);

        // Deep-clone empty state
        const defaultState = JSON.parse(JSON.stringify(emptyState));

        this.state = this.props.account ? {
            ...defaultState,
            account: this.props.account,
            formValid: true,
        } : defaultState;
        this.label = props.account ? 'Edit account' : 'Create new user';
        this.okLabel = props.account ? 'Save' : 'Create';
    }

    componentDidMount() {
        this.props.fetchSkills();
    }

    updateValue = ({ target }) => {
        const { name, value } = target;
        const { errors } = this.state;
        let valid;
        let formattedValue = value;

        switch (name) {
            case 'username':
                valid = value.length > 2;
                errors.username = valid ? '' : 'Username is too short';
                break;
            case 'password':
                valid = value.length > 6;
                errors.password = valid ? '' : 'Password is too short';
                break;
            case 'nickname':
                valid = value.length > 2;
                errors.nickname = valid ? '' : 'Screen name is too short';
                break;
            case 'phone':
                valid = /^\+?[0-9()-]+$/.test(value);
                errors.phone = valid ? '' : 'Invalid phone number';
                break;
            case 'email':
                valid = /\S+@\S+\.\S+/.test(value);
                errors.email = valid ? '' : 'Invalid email address';
                break;
            case 'birthYear':
                formattedValue = parseInt(value, 10) || value;
                valid = value === '' ||
                        /^\d{4}$/.test(value) &&
                        formattedValue >= 1900 &&
                        formattedValue <= new Date().getFullYear();
                errors.birthYear = valid ? '' : 'Invalid birth year';
                break;
            default:
                break;
        }

        // Reset error for an emptied field
        if (value.length === 0) {
            errors[name] = '';
        }

        this.setState(prevState => ({
            account: {
                ...prevState.account,
                [target.name]: formattedValue,
            },
            errors,
            formValid: Object.values(errors).every(e => e === ''),
        }));
    }

    changeSkills = (selectedSkills) => {
        if (this.state.account.skills.includes(selectedSkills)) {
            this.removeSelectedSkills(selectedSkills);
        } else {
            this.addSelectedSkills(selectedSkills);
        }
    }

    addSelectedSkills(skill) {
        this.setState(prevState => ({
            account: {
                ...prevState.account,
                skills: [...prevState.account.skills, skill],
            },
            inputSkills: '',
        }));
    }

    removeSelectedSkills = (skill) => {
        this.setState(prevState => ({
            inputSkills: '',
            account: {
                ...prevState.account,
                skills: prevState.account.skills.filter(
                    i => i !== skill,
                ),
            },
        }));
    }

    changeSkillsInput = (inputVal) => {
        const { skills } = this.state.account;
        const t = inputVal.split(',');

        if (JSON.stringify(t) !== JSON.stringify(skills)) {
            this.setState({ inputSkills: inputVal });
        }
    }

    changeLanguages = (selectedLanguages) => {
        if (this.state.account.skills.includes(selectedLanguages)) {
            this.removeSelectedLanguages(selectedLanguages);
        } else {
            this.addSelectedLanguages(selectedLanguages);
        }
    }

    addSelectedLanguages(language) {
        this.setState(prevState => ({
            account: {
                ...prevState.account,
                languages: [...prevState.account.languages, language],
            },
            inputLanguages: '',
        }));
    }

    removeSelectedLanguages = (language) => {
        this.setState(prevState => ({
            inputLanguages: '',
            account: {
                ...prevState.account,
                languages: prevState.account.languages.filter(
                    i => i !== language,
                ),
            },
        }));
    }

    changeLanguagesInput = (inputVal) => {
        const { languages } = this.state.account;
        const t = inputVal.split(',');

        if (JSON.stringify(t) !== JSON.stringify(languages)) {
            this.setState({ inputLanguages: inputVal });
        }
    }

    updateCheckboxes = checkboxes => ({ target }, checked) => {
        this.setState((prevState) => {
            let values = prevState.account[checkboxes];

            if (checked) {
                values = [...new Set([...values, target.value])];
            } else {
                values = values.filter(v => v !== target.value);
            }

            return {
                account: { ...prevState.account, [checkboxes]: values },
            };
        });
    }

    updateTabs = (event, selectedTab) => {
        this.setState({ selectedTab });
    };

    sendAccount = () => {
        const errors = {};

        // Username is required
        if (!this.state.account.username) {
            errors.username = 'Username is required';
        }

        // Password is required for new accounts
        if (!this.state.account.password && !this.props.account) {
            errors.password = 'Password is required';
        }

        if (Object.keys(errors).length > 0) {
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    ...errors,
                },
            }));

            return;
        }

        if ('id' in this.state.account) {
            delete this.state.account.password;

            this.props.updateAccount(this.state.account);
        } else {
            this.props.createAccount(this.state.account);
        }

        this.props.closeDialog();
    }

    render() {
        const {
            open, availableSkills, isFetchingSkills, fullScreen,
        } = this.props;
        const { selectedTab, account, formValid, errors } = this.state;
        const isMentor = account.role === UserRoles.MENTOR;

        if (isFetchingSkills) {
            return <Loading fullScreen />;
        }

        return (
            <Dialog
                open={open}
                fullScreen={fullScreen}
                fullWidth
                onClose={this.props.closeDialog}
            >
                <DialogTitle>{this.label}</DialogTitle>
                <DialogContent>
                    <Tabs
                        value={selectedTab}
                        fullWidth
                        onChange={this.updateTabs}
                    >
                        <Tab label="User account" />
                        <Tab label="Mentor profile" disabled={!isMentor} />
                    </Tabs>
                </DialogContent>
                <DialogContent>
                    {selectedTab === 0 &&
                        <AccountForm
                            account={account}
                            isNew={!this.props.account}
                            errors={errors}
                            onValueChange={this.updateValue}
                        />
                    }
                    {selectedTab === 1 &&
                        <MentorForm
                            account={account}
                            skillsInputValue={this.state.inputSkills}
                            languagesInputValue={this.state.inputLanguages}
                            selectedSkills={this.state.account.skills}
                            selectedLanguages={this.state.account.languages}
                            availableSkills={availableSkills}
                            availableLanguages={Languages}
                            errors={errors}
                            onValueChange={this.updateValue}
                            onCheckboxesChange={this.updateCheckboxes}
                            onSkillsChange={this.changeSkills}
                            onSkillsInputChange={this.changeSkillsInput}
                            onSkillsRemove={this.removeSelectedSkills}
                            onLanguagesChange={this.changeLanguages}
                            onLanguagesInputChange={this.changeLanguagesInput}
                            onLanguagesRemove={this.removeSelectedLanguages}
                        />
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.closeDialog}>Cancel</Button>
                    <Button
                        disabled={!formValid}
                        color="secondary"
                        onClick={this.sendAccount}
                    >
                        {this.okLabel}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

function mapStateToProps({ skills }) {
    return {
        availableSkills: skills.items,
        isFetchingSkills: skills.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchSkills, createAccount, updateAccount, closeDialog,
})(withMobileDialog()(AccountDialog));
