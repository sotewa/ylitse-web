import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { version as uiVersion } from '../../package.json';
import { UserRoles } from '../constants';
import { fetchVersion, logout } from '../actions';
import TopBar from '../components/TopBar';

const adminToolbarLinks = [{
    label: 'Accounts',
    component: props => <Link to="/accounts" {...props} />,
}, {
    label: 'Skills',
    component: props => <Link to="/skills" {...props} />,
}];

export class Navigation extends Component {
    static propTypes = {
        apiVersion: PropTypes.string.isRequired,
        username: PropTypes.string.isRequired,
        role: PropTypes.string.isRequired,
        fetchVersion: PropTypes.func.isRequired,
        logout: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.fetchVersion();
    }

    render() {
        const { apiVersion, username, role } = this.props;
        const toolbarLinks = role === UserRoles.ADMIN ? adminToolbarLinks : [];
        let fullUiVersion = uiVersion;

        if (process.env.REV) {
            fullUiVersion = `${uiVersion}:${process.env.REV}`;
        }

        return (
            <TopBar
                toolbarLinks={toolbarLinks}
                username={username}
                version={{ ui: fullUiVersion, api: apiVersion || 'N/A' }}
                onLogoutClick={this.props.logout}
            />
        );
    }
}

function mapStateToProps({ version, user }) {
    return {
        apiVersion: version.api,
        role: user.role,
        username: user.username,
    };
}

export default connect(mapStateToProps, { fetchVersion, logout })(Navigation);
