import React from 'react';
import { shallow } from 'enzyme';

import MentorForm from './MentorForm';

describe('MentorForm', () => {
    const props = {
        account: {
            gender: 'male',
            languages: ['fi'],
            skills: ['Javascript'],
            communicationChannels: ['email'],
            story: '',
        },
        skillsInputValue: '',
        languagesInputValue: '',
        availableSkills: [
            { name: 'Javascript', id: '1' },
            { name: 'Python', id: '2' },
        ],
        availableLanguages: [
            { name: 'English', id: 'en' },
            { name: 'French', id: 'fr' },
        ],
        selectedSkills: [],
        selectedLanguages: [],
        errors: {},
        classes: {},
        onValueChange: () => {},
        onCheckboxesChange: () => {},
        onSkillsChange: () => {},
        onSkillsInputChange: () => {},
        onSkillsRemove: () => {},
        onLanguagesChange: () => {},
        onLanguagesInputChange: () => {},
        onLanguagesRemove: () => {},
    };

    test('renders correctly', () => {
        const wrapper = shallow(<MentorForm {...props} />).dive();
        expect(wrapper).toMatchSnapshot();
    });
});
