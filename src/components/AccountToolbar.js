import React from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import Toolbar from 'material-ui/Toolbar';
import PersonAddIcon from 'material-ui-icons/PersonAdd';

const styles = theme => ({
    toolbar: {
        paddingLeft: 1,
    },
    button: {
        marginTop: theme.spacing.unit * 3,
        marginBottom: theme.spacing.unit * 3,
    },
    buttonIcon: {
        marginLeft: theme.spacing.unit,
    },
});

const AccountToolbar = ({ onAddClick, classes }) => (
    <Toolbar className={classes.toolbar}>
        <Button
            variant="raised"
            color="secondary"
            className={classes.button}
            onClick={onAddClick}
        >
            Add new
            <PersonAddIcon className={classes.buttonIcon} />
        </Button>
    </Toolbar>
);

AccountToolbar.propTypes = {
    onAddClick: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        toolbar: PropTypes.string,
        input: PropTypes.string,
    }).isRequired,
};

export default withStyles(styles)(AccountToolbar);
