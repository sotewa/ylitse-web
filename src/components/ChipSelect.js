import React from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import Chip from 'material-ui/Chip';
import { MenuItem } from 'material-ui/Menu';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import CancelIcon from 'material-ui-icons/Cancel';

const styles = theme => ({
    mainContainer: {
        position: 'relative',
    },
    chipContainer: {
        backgroundColor: 'transparent',
        display: 'inline-block',
        marginTop: theme.spacing.unit,
    },
    chip: {
        marginRight: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
    paper: {
        position: 'absolute',
        left: 0,
        right: 0,
        zIndex: 1,
        maxHeight: 150,
        overflowY: 'auto',
    },
});

const renderInput = (inputProps) => {
    const {
        InputProps, classes, selectedItem, availableItems, itemName, ...other
    } = inputProps;

    const allItemsSelected = selectedItem.length === availableItems.length;
    const placeholder = allItemsSelected
                      ? `No ${itemName}s to add` : `Add a ${itemName}`;

    return (
        <TextField
            fullWidth
            placeholder={placeholder}
            disabled={allItemsSelected}
            {...other}
            InputProps={{
                classes: { input: classes.input },
                ...InputProps,
            }}
        />
    );
};

const renderChipList = (inputProps) => {
    const {
        InputProps, classes, selectedItem, onRemoveItem,
    } = inputProps;
    return (
        <div className={classes.chipContainer} {...InputProps}>
            {selectedItem.length > 0
            && selectedItem.map(item => (
                <Chip
                    key={item}
                    className={classes.chip}
                    label={item}
                    deleteIcon={<CancelIcon />}
                    onDelete={() => onRemoveItem(item)}
                />
            ))}
        </div>
    );
};

const renderSuggestion = (params) => {
    const {
        item, index, itemProps, highlightedIndex, selectedItem,
    } = params;
    const isHighlighted = highlightedIndex === index;
    const isSelected = selectedItem.indexOf(item.name) > -1;

    return !isSelected &&
        <MenuItem
            {...itemProps}
            key={item.id}
            selected={isHighlighted}
            component="div"
        >
            {item.name}
        </MenuItem>;
};

const getSuggestions = (value, list) => list.filter(item =>
    item.name.toLowerCase().includes(value.toLowerCase()));

const renderResults = (
    inputValue, availableItems, highlightedIndex, selectedItem, getItemProps,
    classes,
) => (
    <Paper className={classes.paper} square>
        {getSuggestions(inputValue, availableItems).map((item, index) =>
            renderSuggestion({
                item,
                index,
                itemProps: getItemProps({
                    item: item.name,
                }),
                highlightedIndex,
                selectedItem,
            }))
        }
    </Paper>
);

function ChipSelect(props) {
    const {
        classes, availableItems, onRemoveItem, itemName, ...rest
    } = props;

    return (
        <Downshift defaultHighlightedIndex={0} {...rest}>
            {({
                getInputProps,
                getItemProps,
                inputValue,
                selectedItem,
                highlightedIndex,
                toggleMenu,
                isOpen,
            }) => (
                <div className={classes.mainContainer}>
                    {renderChipList({
                        classes,
                        onRemoveItem,
                        selectedItem,
                        InputProps: { ...getInputProps() },
                    })}
                    {renderInput({
                        classes,
                        selectedItem,
                        availableItems,
                        itemName,
                        InputProps: getInputProps({
                            onClick: () => toggleMenu(),
                        }),
                    })}
                    {isOpen && renderResults(
                        inputValue, availableItems, highlightedIndex,
                        selectedItem, getItemProps, classes,
                    )}
                </div>
            )}
        </Downshift>
    );
}

ChipSelect.propTypes = {
    availableItems: PropTypes.arrayOf(PropTypes.object).isRequired,
    itemName: PropTypes.string.isRequired,
    classes: PropTypes.shape({
        chipContainer: PropTypes.string,
        paper: PropTypes.string,
    }).isRequired,
    onRemoveItem: PropTypes.func.isRequired,
};

export default withStyles(styles)(ChipSelect);
