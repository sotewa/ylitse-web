import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { shallow } from 'enzyme';

import { Navigation } from './Navigation';

describe('Navigation', () => {
    const props = {
        apiVersion: '0.1',
        username: 'test',
        role: 'test',
        fetchVersion: () => {},
        logout: () => {},
    };
    const wrapper = shallow(
        <MemoryRouter><Navigation {...props} /></MemoryRouter>,
    ).dive().dive().shallow();

    test('renders correctly', () => {
        expect(wrapper.find('TopBar')).toHaveLength(1);
    });
});
