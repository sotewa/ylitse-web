import React from 'react';
import PropTypes from 'prop-types';
import GridList from 'material-ui/GridList';
import Typography from 'material-ui/Typography';

import SkillListItem from './SkillListItem';

const SkillList = ({ skills, onDelete }) => (
    <main>
        {skills.length > 0 &&
            <GridList>
                {skills.map(skill => (
                    <SkillListItem
                        key={skill.id}
                        label={skill.name}
                        onDelete={onDelete(skill.id)}
                    />
                ))}
            </GridList>
        }
        {skills.length === 0 &&
            <Typography variant="display3">No mentor skills</Typography>
        }
    </main>
);

SkillList.propTypes = {
    skills: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
    })).isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default SkillList;
