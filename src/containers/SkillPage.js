import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { fetchSkills, createSkill, deleteSkill } from '../actions';
import Loading from '../components/Loading';
import Page from '../components/Page';
import SkillToolbar from '../components/SkillToolbar';
import SkillList from '../components/SkillList';
import ConfirmationDialog from '../components/ConfirmationDialog';

export class SkillPage extends Component {
    static propTypes = {
        skills: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })).isRequired,
        isFetching: PropTypes.bool.isRequired,
        fetchSkills: PropTypes.func.isRequired,
        createSkill: PropTypes.func.isRequired,
        deleteSkill: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            newSkillName: '',
            selectedSkill: undefined,
            errors: {},
            confirmationOpen: false,
        };
    }

    componentDidMount() {
        this.props.fetchSkills();
    }

    updateText = ({ target }) => {
        let valid = false;
        let error = '';

        switch (target.name) {
            case 'newSkillName':
                valid = !this.props.skills.some(skill => (
                    skill.name.toLowerCase() === target.value.toLowerCase()
                ));
                error = valid ? '' : 'Already in the list';
                break;
            default:
                break;
        }

        this.setState(prevState => ({
            [target.name]: target.value,
            errors: { ...prevState.errors, [target.name]: error },
        }));
    }

    resetNewSkill = () => {
        if (!this.state.newSkillName) {
            return;
        }

        this.setState(prevState => ({
            newSkillName: '',
            errors: { ...prevState.errors, newSkillName: '' },
        }));
    }

    addAndResetNewSkill = () => {
        if (!this.state.newSkillName || this.state.errors.newSkillName) {
            return;
        }

        this.props.createSkill(this.state.newSkillName);
        this.resetNewSkill(this.state.newSkillName);
    }

    addSkill = (event) => {
        switch (event.key) {
            case 'Enter': {
                event.preventDefault();
                this.addAndResetNewSkill();
                break;
            }
            case 'Escape': {
                event.preventDefault();
                this.resetNewSkill();
                break;
            }
            default:
                break;
        }
    }

    deleteSkill = skill => () => {
        this.props.deleteSkill(skill);
        this.closeConfirmation();
    }

    openConfirmation = id => () => {
        this.setState({
            selectedSkill: this.props.skills.find(s => s.id === id),
            confirmationOpen: true,
        });
    }

    closeConfirmation = () => {
        this.setState({ selectedSkill: undefined, confirmationOpen: false });
    }

    render() {
        const { skills, isFetching } = this.props;
        const {
            newSkillName, selectedSkill, errors, confirmationOpen,
        } = this.state;

        return (
            <Page header="Mentor skills">
                <SkillToolbar
                    disabled={isFetching}
                    addValue={newSkillName}
                    addError={errors.newSkillName}
                    onAddChange={this.updateText}
                    onAddKeyDown={this.addSkill}
                />
                {isFetching &&
                    <Loading />
                }
                {!isFetching &&
                    <SkillList
                        skills={skills}
                        onDelete={this.openConfirmation}
                    />
                }
                {selectedSkill &&
                    <ConfirmationDialog
                        open={confirmationOpen}
                        label={`Really delete ${selectedSkill.name} skill?`}
                        okLabel="Delete"
                        onOkClick={this.deleteSkill(selectedSkill)}
                        onClose={this.closeConfirmation}
                    />
                }
            </Page>
        );
    }
}

function mapStateToProps({ skills }) {
    return {
        skills: skills.items,
        isFetching: skills.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchSkills, createSkill, deleteSkill,
})(SkillPage);
