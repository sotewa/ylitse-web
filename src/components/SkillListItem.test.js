import React from 'react';
import { shallow } from 'enzyme';

import SkillListItem from './SkillListItem';

describe('SkillListItem', () => {
    test('renders correctly', () => {
        const props = { label: 'Test', onDelete: () => {} };

        const wrapper = shallow(<SkillListItem {...props} />).dive();
        expect(wrapper).toMatchSnapshot();
    });
});
