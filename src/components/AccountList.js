import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
    TableRow, TableHead, TableCell, TableBody,
} from 'material-ui/Table';
import Typography from 'material-ui/Typography';

import AccountListItem from './AccountListItem';

const styles = () => ({
    table: {
        overflowX: 'auto',
    },
    firstCell: {
        paddingLeft: 0,
    },
    lastCell: {
        '&:last-child': {
            paddingRight: [0, '!important'],
            textAlign: ['right', '!important'],
        },
    },
});

const AccountList = ({ accounts, onEdit, onDelete, onPassChange, classes }) => (
    <main>
        {accounts.length > 0 &&
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.firstCell}>
                            Username
                        </TableCell>
                        <TableCell>Role</TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell className={classes.lastCell}>
                            Actions
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {accounts.map(account => (
                        <AccountListItem
                            key={account.id}
                            account={account}
                            onEdit={onEdit(account)}
                            onDelete={onDelete(account)}
                            onPassChange={onPassChange(account)}
                        />
                    ))}
                </TableBody>
            </Table>
        }
        {accounts.length === 0 &&
            <Typography variant="display3">No user accounts</Typography>
        }
    </main>
);

AccountList.propTypes = {
    accounts: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
    })).isRequired,
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onPassChange: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        table: PropTypes.string,
        firstCell: PropTypes.string,
        lastCell: PropTypes.string,
    }).isRequired,
};

export default withStyles(styles)(AccountList);
