import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';

import { UserRoles } from '../constants';
import PasswordField from './PasswordField';
import RadioGroupControl from './RadioGroupControl';

const styles = theme => ({
    row: {
        marginBottom: theme.spacing.unit,
    },
    radioRow: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: 0,
    },
    checkboxRow: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: 0,
    },
    formControl: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
    button: {
        marginTop: theme.spacing.unit * 2,
    },
    buttonIcon: {
        marginLeft: theme.spacing.unit,
    },
});

const renderTextField = (name, label, required, other) => (
    <TextField
        name={name}
        label={label}
        value={other.account[name]}
        error={Boolean(other.errors[name])}
        helperText={other.errors[name]}
        required={required}
        className={other.classes.row}
        onChange={other.onValueChange}
    />
);

const AccountForm = props => (
    <form autoComplete="off">
        <FormGroup>
            {props.isNew &&
                <RadioGroupControl
                    name="role"
                    label="Account type"
                    value={props.account.role}
                    options={Object.values(UserRoles)}
                    className={props.classes.radioRow}
                    onChange={props.onValueChange}
                />
            }
            {renderTextField('username', 'Username', true, props)}
            {props.isNew &&
                <PasswordField
                    name="password"
                    label="Password"
                    value={props.account.password}
                    error={Boolean(props.errors.password)}
                    helperText={props.errors.password}
                    required
                    className={props.classes.row}
                    onChange={props.onValueChange}
                />
            }
            {renderTextField('nickname', 'Screen name', false, props)}
            {renderTextField('email', 'Email', false, props)}
        </FormGroup>
    </form>
);

AccountForm.propTypes = {
    account: PropTypes.shape({
        role: PropTypes.string,
        password: PropTypes.string,
    }).isRequired,
    isNew: PropTypes.bool,
    errors: PropTypes.shape({
        password: PropTypes.string,
    }).isRequired,
    onValueChange: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        row: PropTypes.string,
        radioRow: PropTypes.string,
        checkboxRow: PropTypes.string,
        formControl: PropTypes.string,
        button: PropTypes.string,
        buttonIcon: PropTypes.string,
    }).isRequired,
};

AccountForm.defaultProps = {
    isNew: false,
};

export default withStyles(styles)(AccountForm);
