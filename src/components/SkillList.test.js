import React from 'react';
import { shallow } from 'enzyme';

import SkillList from './SkillList';

describe('SkillList', () => {
    test('renders correctly', () => {
        const props = {
            skills: [{ id: '0', name: 'Test' }],
            onDelete: () => {},
        };

        const wrapper = shallow(<SkillList {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});
