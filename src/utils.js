export function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export function camelify(str) {
    return str.replace(/_(\w|$)/g, (_, ch) => ch.toUpperCase());
}

export function snakify(str) {
    return str.replace(/([a-z\d])([A-Z])/g, '$1_$2').toLowerCase();
}

export function updateKeys(obj, func) {
    return Object.keys(obj).reduce((updated, key) => ({
        ...updated,
        [func(key)]: obj[key],
    }), {});
}

export function updateWith(item, items) {
    const id = items.findIndex(a => a.id === item.id);

    return [...items.slice(0, id), item, ...items.slice(id + 1)];
}

export function sortBy(key, items) {
    return items.sort((a, b) => {
        const valueA = a[key] || '';
        const valueB = b[key] || '';

        return valueA.toLowerCase().localeCompare(valueB.toLowerCase());
    });
}
