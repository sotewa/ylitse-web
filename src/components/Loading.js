import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
    inline: {
        position: 'relative',
        left: '50%',
        transform: 'translateX(-50%)',
        marginTop: '10%',
        marginBottom: '10%',
    },
    fullScreen: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translateX(-50%) translateY(-50%)',
        backgroundColor: theme.palette.grey[100],
        padding: 5,
        borderRadius: 5,
    },
});

class Loading extends Component {
    static propTypes = {
        fullScreen: PropTypes.bool,
        classes: PropTypes.shape({
            outer: PropTypes.string,
            progress: PropTypes.string,
        }).isRequired,
    }

    static defaultProps = {
        fullScreen: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        };
    }

    componentDidMount() {
        this.timer = setTimeout(() => {
            this.setState({ visible: true });
        }, 200);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        const { fullScreen, classes } = this.props;
        const { visible } = this.state;
        const className = fullScreen ? classes.fullScreen : classes.inline;

        if (!visible) {
            return null;
        }

        return (
            <CircularProgress
                className={className}
                size={50}
                color="secondary"
            />
        );
    }
}

export default withStyles(styles)(Loading);
