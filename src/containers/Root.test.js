import React from 'react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { mount } from 'enzyme';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import { UserRoles } from '../constants';
import { Root } from './Root';

jest.mock('react-router-dom/BrowserRouter', () => (
    jest.fn(({ children }) => <div>{children}</div>)
));

describe('Root', () => {
    const mockStore = configureStore([thunk]);
    const state = {
        user: { role: UserRoles.ADMIN, username: 'test' },
        accounts: { items: [], isFetching: false },
        skills: { items: [], isFetching: false },
        feedback: { current: '', messages: [] },
        dialog: { type: '', props: {} },
        version: { api: '' },
    };
    let props = { ...state.user, fetchUser: () => {} };
    let wrapper = mount(
        <Provider store={mockStore(state)}>
            <MemoryRouter initialEntries={['/']}>
                <Root {...props} />
            </MemoryRouter>
        </Provider>,
    );
    const { history } = wrapper.find('Router').props();

    test('renders default route correctly', () => {
        expect(wrapper.find('AccountPage')).toHaveLength(1);
    });

    test('renders accounts route correctly', () => {
        history.push('/accounts');
        wrapper.update();
        expect(wrapper.find('AccountPage')).toHaveLength(1);
    });

    test('renders skills route correctly', () => {
        history.push('/skills');
        wrapper.update();
        expect(wrapper.find('SkillPage')).toHaveLength(1);
    });

    test('renders invalid route correctly', () => {
        history.push('/invalid');
        wrapper.update();
        expect(wrapper.find('MissingPage')).toHaveLength(1);
    });

    test('renders nothing if user is missing', () => {
        props = {
            username: '',
            role: '',
            fetchUser: () => {},
        };
        wrapper = mount(
            <Provider store={mockStore(state)}>
                <Root {...props} />
            </Provider>,
        );

        expect(wrapper.find('Feedback[critical]')).toHaveLength(1);
    });
});
