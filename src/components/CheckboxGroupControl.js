import React from 'react';
import PropTypes from 'prop-types';
import {
    FormControl, FormLabel, FormControlLabel, FormGroup,
} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';
import { withStyles } from 'material-ui/styles';

const styles = () => ({
    label: {
        textTransform: 'capitalize',
    },
});

const CheckboxGroupControl = ({
    label, options, checkedKeys, disabled, required, className, classes,
    onChange,
}) => (
    <FormControl
        component="fieldset"
        disabled={disabled}
        required={required}
        className={className}
    >
        <FormLabel component="legend">{label}</FormLabel>
        <FormGroup row>
            {Object.keys(options).map(key => (
                <FormControlLabel
                    key={key}
                    label={options[key]}
                    value={key}
                    checked={checkedKeys.includes(key)}
                    control={<Checkbox onChange={onChange} />}
                    className={classes.label}
                />
            ))}
        </FormGroup>
    </FormControl>
);

CheckboxGroupControl.propTypes = {
    label: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    options: PropTypes.object.isRequired,
    checkedKeys: PropTypes.arrayOf(PropTypes.string),
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    className: PropTypes.string.isRequired,
    classes: PropTypes.shape({ label: PropTypes.string }).isRequired,
    onChange: PropTypes.func.isRequired,
};

CheckboxGroupControl.defaultProps = {
    checkedKeys: [],
    disabled: false,
    required: false,
};

export default withStyles(styles)(CheckboxGroupControl);
