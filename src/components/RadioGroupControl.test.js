import React from 'react';
import { shallow } from 'enzyme';
import RadioGroupControl from './RadioGroupControl';

describe('RadioGroupControl', () => {
    test('renders correctly', () => {
        const props = {
            name: 'test',
            label: 'Test label',
            value: 'One',
            options: ['One', 'Two'],
            onChange: () => {},
        };

        const wrapper = shallow(<RadioGroupControl {...props} />).dive();
        expect(wrapper).toMatchSnapshot();

        wrapper.setProps({ required: true });
        expect(wrapper).toMatchSnapshot();

        wrapper.setProps({ disabled: true });
        expect(wrapper).toMatchSnapshot();
    });
});
