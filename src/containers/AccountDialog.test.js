import React from 'react';
import { shallow } from 'enzyme';

import { AccountDialog } from './AccountDialog';

describe('AccountDialog', () => {
    test('renders correctly', () => {
        const props = {
            open: true,
            fullScreen: false,
            availableSkills: [{ id: '0', name: 'Test skill' }],
            isFetchingSkills: true,
            fetchSkills: () => {},
            createAccount: () => {},
            updateAccount: () => {},
            closeDialog: () => {},
        };

        const wrapper = shallow(<AccountDialog {...props} />);
        expect(wrapper).toMatchSnapshot();

        wrapper.setProps({ isFetchingSkills: false });
        expect(wrapper).toMatchSnapshot();
    });
});
