import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog, {
    DialogActions, DialogContent, DialogTitle,
} from 'material-ui/Dialog';
import { FormGroup } from 'material-ui/Form';

import { updateAccount, closeDialog } from '../actions';
import PasswordField from '../components/PasswordField';

export class PasswordDialog extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        account: PropTypes.shape({
            username: PropTypes.string,
        }).isRequired,
        updateAccount: PropTypes.func.isRequired,
        closeDialog: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            account: this.props.account,
            errors: {
                password: undefined,
                password2: undefined,
            },
            formValid: false,
        };
    }

    updateValue = ({ target }) => {
        const { name, value } = target;
        const { errors } = this.state;

        if (name === 'password') {
            const valid = value.length > 6;
            errors.password = valid ? '' : 'Password is too short';
        }

        this.setState((prevState) => {
            const { password, password2 } = prevState.account;

            if (name === 'password' && password2 === value ||
                name === 'password2' && password === value) {
                errors.password2 = '';
            } else {
                errors.password2 = 'Passwords don\'t match';
            }

            return {
                account: {
                    ...prevState.account,
                    [target.name]: value,
                },
                errors,
                formValid: Object.values(errors).every(e => e === ''),
            };
        });
    }

    sendPassword = () => {
        delete this.state.account.password2;

        this.props.updateAccount(this.state.account);
        this.props.closeDialog();
    }

    render() {
        const { open } = this.props;
        const { account, formValid, errors } = this.state;

        return (
            <Dialog open={open} onClose={this.props.closeDialog}>
                <DialogTitle>Set password for {account.username}</DialogTitle>
                <DialogContent>
                    <FormGroup>
                        <PasswordField
                            name="password"
                            label="Password"
                            error={Boolean(errors.password)}
                            helperText={errors.password}
                            required
                            style={{ marginBottom: 8 }}
                            onChange={this.updateValue}
                        />
                        <PasswordField
                            name="password2"
                            label="Confirm password"
                            error={Boolean(errors.password2)}
                            helperText={errors.password2}
                            required
                            style={{ marginBottom: 8 }}
                            onChange={this.updateValue}
                        />
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.closeDialog}>Cancel</Button>
                    <Button
                        color="secondary"
                        disabled={!formValid}
                        onClick={this.sendPassword}
                    >
                        Set
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default connect(null, { updateAccount, closeDialog })(PasswordDialog);
