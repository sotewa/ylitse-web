import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, FormGroup, FormLabel } from 'material-ui/Form';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';

import { UserGenders } from '../constants';
import { AvailableChannels } from '../containers/AccountDialog';
import CheckboxGroupControl from './CheckboxGroupControl';
import ChipSelect from './ChipSelect';
import RadioGroupControl from './RadioGroupControl';

const styles = theme => ({
    row: {
        marginBottom: theme.spacing.unit,
    },
    radioRow: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: 0,
    },
    checkboxRow: {
        marginTop: theme.spacing.unit * 2,
        marginBottom: 0,
    },
    formControl: {
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit,
    },
    legend: {
        marginBottom: theme.spacing.unit,
    },
    button: {
        marginTop: theme.spacing.unit * 2,
    },
    buttonIcon: {
        marginLeft: theme.spacing.unit,
    },
});

const renderTextField = (name, label, required, other) => (
    <TextField
        name={name}
        label={label}
        value={other.account[name]}
        error={Boolean(other.errors[name])}
        helperText={other.errors[name]}
        required={required}
        className={other.classes.row}
        onChange={other.onValueChange}
    />
);

const MentorForm = props => (
    <form autoComplete="off">
        <FormGroup>
            {renderTextField('phone', 'Phone number', false, props)}
            <RadioGroupControl
                name="gender"
                label="Gender"
                value={props.account.gender}
                options={Object.values(UserGenders)}
                className={props.classes.radioRow}
                onChange={props.onValueChange}
            />
            {renderTextField('birthYear', 'Birth year', false, props)}
            {renderTextField('area', 'Area', false, props)}
            <FormControl
                disabled={props.availableSkills.length === 0}
                className={props.classes.formControl}
            >
                <FormLabel component="legend" className={props.classes.legend}>
                    Languages
                </FormLabel>
                <ChipSelect
                    itemName="language"
                    inputValue={props.languagesInputValue}
                    selectedItem={props.selectedLanguages}
                    availableItems={props.availableLanguages}
                    onChange={props.onLanguagesChange}
                    onRemoveItem={props.onLanguagesRemove}
                    onInputValueChange={props.onLanguagesInputChange}
                />
            </FormControl>
            <FormControl
                disabled={props.availableSkills.length === 0}
                className={props.classes.formControl}
            >
                <FormLabel component="legend" className={props.classes.legend}>
                    Skills
                </FormLabel>
                <ChipSelect
                    itemName="skill"
                    inputValue={props.skillsInputValue}
                    selectedItem={props.selectedSkills}
                    availableItems={props.availableSkills}
                    onChange={props.onSkillsChange}
                    onRemoveItem={props.onSkillsRemove}
                    onInputValueChange={props.onSkillsInputChange}
                />
            </FormControl>
            <CheckboxGroupControl
                label="Communication channels"
                options={AvailableChannels}
                checkedKeys={props.account.communicationChannels}
                className={props.classes.checkboxRow}
                onChange={props.onCheckboxesChange('communicationChannels')}
            />
            <TextField
                name="story"
                label="Story"
                color="secondary"
                value={props.account.story}
                helperText="Tell something about yourself"
                multiline
                rows={1}
                rowsMax={8}
                className={props.classes.row}
                onChange={props.onValueChange}
            />
        </FormGroup>
    </form>
);

MentorForm.propTypes = {
    account: PropTypes.shape({
        gender: PropTypes.string,
        languages: PropTypes.arrayOf(PropTypes.string),
        skills: PropTypes.arrayOf(PropTypes.string),
        communicationChannels: PropTypes.arrayOf(PropTypes.string),
        story: PropTypes.string,
    }).isRequired,
    skillsInputValue: PropTypes.string.isRequired,
    languagesInputValue: PropTypes.string.isRequired,
    selectedSkills: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedLanguages: PropTypes.arrayOf(PropTypes.string).isRequired,
    availableSkills: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
    })).isRequired,
    availableLanguages: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
    })).isRequired,
    classes: PropTypes.shape({
        row: PropTypes.string,
        radioRow: PropTypes.string,
        checkboxRow: PropTypes.string,
        formControl: PropTypes.string,
        legend: PropTypes.string,
        button: PropTypes.string,
        buttonIcon: PropTypes.string,
    }).isRequired,
    onValueChange: PropTypes.func.isRequired,
    onCheckboxesChange: PropTypes.func.isRequired,
    onSkillsChange: PropTypes.func.isRequired,
    onSkillsInputChange: PropTypes.func.isRequired,
    onSkillsRemove: PropTypes.func.isRequired,
    onLanguagesChange: PropTypes.func.isRequired,
    onLanguagesInputChange: PropTypes.func.isRequired,
    onLanguagesRemove: PropTypes.func.isRequired,
};

export default withStyles(styles)(MentorForm);
