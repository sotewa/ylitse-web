import React from 'react';
import { shallow, mount } from 'enzyme';

import PasswordField from './PasswordField';

describe('PasswordField', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<PasswordField />).dive();
        expect(wrapper).toMatchSnapshot();
    });

    test('icon button toggles the visibility', () => {
        const wrapper = mount(<PasswordField />);
        expect(wrapper.find('Input').prop('type')).toBe('password');

        wrapper.find('IconButton').simulate('click');
        expect(wrapper.find('Input').prop('type')).toBe('text');
    });

    test('helper text is displayed', () => {
        const wrapper = mount(<PasswordField />);

        wrapper.setProps({ helperText: 'Not a good password' });
        expect(wrapper.find('FormHelperText').length).toBe(1);
    });
});
