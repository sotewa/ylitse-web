import React from 'react';
import { shallow } from 'enzyme';

import Loading from './Loading';

jest.useFakeTimers();

describe('Loading', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<Loading />).dive();
        expect(wrapper).toMatchSnapshot();

        jest.runAllTimers();
        wrapper.update();
        expect(wrapper.dive()).toMatchSnapshot();

        wrapper.setProps({ fullScreen: true });
        expect(wrapper.dive()).toMatchSnapshot();

        wrapper.unmount();
        expect(clearTimeout).toBeCalled();
    });
});
