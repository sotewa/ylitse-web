import React from 'react';
import { shallow } from 'enzyme';

import { PasswordDialog } from './PasswordDialog';

describe('PasswordDialog', () => {
    test('renders correctly', () => {
        const props = {
            open: true,
            account: {
                username: 'Test',
            },
            updateAccount: () => {},
            closeDialog: () => {},
        };

        const wrapper = shallow(<PasswordDialog {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});
