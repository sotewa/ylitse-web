import React from 'react';
import { shallow } from 'enzyme';

import AccountListItem from './AccountListItem';

describe('AccountListItem', () => {
    test('renders correctly', () => {
        const props = {
            account: {
                username: 'test',
                role: 'admin',
                email: 'admin@example.com',
            },
            onEdit: () => {},
            onDelete: () => {},
            onPassChange: () => {},
        };

        const wrapper = shallow(<AccountListItem {...props} />).dive();
        expect(wrapper).toMatchSnapshot();
    });

    test('callbacks work', () => {
        const onEdit = jest.fn();
        const onDelete = jest.fn();
        const onPassChange = jest.fn();
        const props = { account: {}, onEdit, onDelete, onPassChange };
        const wrapper = shallow(<AccountListItem {...props} />).dive();

        const editButton = wrapper.find('[name="edit"]');
        editButton.simulate('click');
        expect(onEdit).toBeCalled();

        const passButton = wrapper.find('[name="pass"]');
        passButton.simulate('click');
        expect(onPassChange).toBeCalled();

        const deleteButton = wrapper.find('[name="delete"]');
        deleteButton.simulate('click');
        expect(onDelete).toBeCalled();
    });
});
