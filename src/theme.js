import { createMuiTheme } from 'material-ui/styles';

export const GREEN_PRIMARY = '#00cca8';
export const GREEN_SECONDARY = '#2aa08e';
export const ORANGE = '#f67f5d';
export const BEIGE = '#ffefe4';
export const YELLOW = '#ffc952';
export const GREY_VERY_LIGHT = '#f4f5f5';
export const GREY_LIGHT = '#f0f1f1';
export const GREY_10 = '#e6e7e8';
export const GREY_30 = '#c0c0c0';
export const GREY_50 = '#888888';
export const GREY_DARK = '#505050';
export const WHITE = '#fff';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: ORANGE,
            contrastText: WHITE,
        },
        secondary: {
            main: GREEN_PRIMARY,
            dark: GREEN_SECONDARY,
            contrastText: WHITE,
        },
        contrastThreshold: 2,
        tonalOffset: 0.1,
        background: {
            default: GREY_VERY_LIGHT,
        },
        text: {
            primary: GREY_DARK,
            secondary: GREY_50,
            disabled: GREY_30,
            hint: GREY_30,
            divider: GREY_10,
        },
    },
    overrides: {
        MuiFormLabel: {
            focused: {
                color: GREEN_SECONDARY,
            },
        },
        MuiInput: {
            inkbar: {
                '&:after': {
                    backgroundColor: GREEN_SECONDARY,
                },
            },
            // Make disabled (placeholder) text readable
            disabled: {
                color: 'inherit',
            },
        },
        MuiTab: {
            fullWidth: {
                // https://github.com/mui-org/material-ui/issues/10284
                maxWidth: 'initial',
            },
        },
    },
});

export default theme;
