import React from 'react';
import { shallow } from 'enzyme';

import AccountForm from './AccountForm';

describe('AccountForm', () => {
    const props = {
        account: {
            role: 'mentee',
            password: '123',
        },
        errors: {
            password: 'Password is too short',
        },
        classes: {},
        onValueChange: () => {},
    };

    test('renders correctly', () => {
        const wrapper = shallow(<AccountForm {...props} />).dive();
        expect(wrapper).toMatchSnapshot();

        wrapper.setProps({ isNew: true });
        expect(wrapper).toMatchSnapshot();
    });
});
