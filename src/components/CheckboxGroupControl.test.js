import React from 'react';
import { shallow, mount } from 'enzyme';
import CheckboxGroupControl from './CheckboxGroupControl';

describe('CheckboxGroupControl', () => {
    const mockFunc = jest.fn();
    const props = {
        options: { fi: 'finnish' },
        checkedKeys: ['fi'],
        className: '',
        label: 'Language',
        onChange: mockFunc,
    };

    test('renders correctly', () => {
        const wrapper = shallow(<CheckboxGroupControl {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('checkbox can be checked and unchecked', () => {
        const wrapper = mount(<CheckboxGroupControl {...props} />);
        const input = wrapper.find('input[value="fi"]');

        input.simulate('change');
        expect(mockFunc).toHaveBeenCalled();
    });
});
