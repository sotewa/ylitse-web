import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

// eslint-disable-next-line import/no-named-as-default
import AccountDialog from './AccountDialog';
// eslint-disable-next-line import/no-named-as-default
import PasswordDialog from './PasswordDialog';

export const DialogTypes = {
    ADD_ACCOUNT: 'ADD_ACCOUNT',
    EDIT_ACCOUNT: 'EDIT_ACCOUNT',
    SET_PASSWORD: 'SET_PASSWORD',
};

const DialogWindow = ({ type, props }) => {
    const open = Boolean(type);

    if (type === DialogTypes.ADD_ACCOUNT
        || type === DialogTypes.EDIT_ACCOUNT) {
        return <AccountDialog open={open} {...props} />;
    }

    if (type === DialogTypes.SET_PASSWORD) {
        return <PasswordDialog open={open} {...props} />;
    }

    return null;
};

DialogWindow.propTypes = {
    type: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    props: PropTypes.object.isRequired,
};

export default connect(state => state.dialog)(DialogWindow);
