import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Toolbar from 'material-ui/Toolbar';

const styles = theme => ({
    toolbar: {
        paddingLeft: 0,
    },
    input: {
        marginTop: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 3,
    },
});

const SkillToolbar = props => (
    <Toolbar className={props.classes.toolbar}>
        <TextField
            name="newSkillName"
            label="Add a skill"
            value={props.addValue}
            disabled={props.disabled}
            className={props.classes.input}
            error={Boolean(props.addError)}
            helperText={props.addError}
            onChange={props.onAddChange}
            onKeyDown={props.onAddKeyDown}
        />
    </Toolbar>
);

SkillToolbar.propTypes = {
    disabled: PropTypes.bool,
    addValue: PropTypes.string.isRequired,
    addError: PropTypes.string,
    onAddChange: PropTypes.func.isRequired,
    onAddKeyDown: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        toolbar: PropTypes.string,
        input: PropTypes.string,
    }).isRequired,
};

SkillToolbar.defaultProps = {
    disabled: false,
    addError: '',
};

export default withStyles(styles)(SkillToolbar);
