import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Menu, { MenuItem } from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui-icons/Info';
import MoreVertIcon from 'material-ui-icons/MoreVert';

import AboutDialog from './AboutDialog';

const styles = () => ({
    flex: {
        flex: 1,
    },
    iconButton: {
        marginLeft: 20,
        marginRight: -24,
    },
});

class TopBar extends Component {
    static propTypes = {
        username: PropTypes.string.isRequired,
        toolbarLinks: PropTypes.arrayOf(PropTypes.object).isRequired,
        version: PropTypes.shape({
            ui: PropTypes.string,
            api: PropTypes.string,
        }).isRequired,
        onLogoutClick: PropTypes.func.isRequired,
        classes: PropTypes.shape({
            flex: PropTypes.string,
            infoButton: PropTypes.string,
        }).isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            aboutOpen: false,
            menuAnchor: null,
        };
    }

    openAbout = () => {
        this.setState({ aboutOpen: true });
    }

    closeAbout = () => {
        this.setState({ aboutOpen: false });
    }

    openMenu = (event) => {
        this.setState({ menuAnchor: event.currentTarget });
    };

    closeMenu = () => {
        this.setState({ menuAnchor: null });
    };

    render() {
        const {
            toolbarLinks, username, version, classes, onLogoutClick,
        } = this.props;
        const { aboutOpen, menuAnchor } = this.state;

        return (
            <AppBar position="static">
                <Toolbar display="flex">
                    <Typography
                        variant="title"
                        color="inherit"
                        className={classes.flex}
                    >
                        Ylitse Admin
                    </Typography>
                    {toolbarLinks.map(link => (
                        <Button
                            key={link.label}
                            component={link.component}
                            color="inherit"
                        >
                            {link.label}
                        </Button>
                    ))}
                    <IconButton
                        name="about"
                        color="inherit"
                        className={classes.iconButton}
                        onClick={this.openAbout}
                    >
                        <InfoIcon />
                    </IconButton>
                    <IconButton
                        name="menu"
                        aria-owns={menuAnchor ? 'menu-appbar' : null}
                        aria-haspopup="true"
                        color="inherit"
                        className={classes.iconButton}
                        onClick={this.openMenu}
                    >
                        <MoreVertIcon />
                    </IconButton>
                </Toolbar>
                <Menu
                    open={Boolean(menuAnchor)}
                    id="menu-appbar"
                    anchorEl={menuAnchor}
                    onClose={this.closeMenu}
                >
                    <MenuItem onClick={onLogoutClick}>
                        Sign out {username}
                    </MenuItem>
                </Menu>
                <AboutDialog
                    open={aboutOpen}
                    uiVersion={version.ui}
                    apiVersion={version.api}
                    onOkClick={this.closeAbout}
                    onClose={this.closeAbout}
                />
            </AppBar>
        );
    }
}

export default withStyles(styles)(TopBar);
