import React from 'react';
import { shallow } from 'enzyme';

import { AccountPage } from './AccountPage';

describe('AccountPage', () => {
    test('renders correctly', () => {
        const props = {
            accounts: [{ id: '0', username: 'test' }],
            isFetching: false,
            fetchAccounts: () => {},
            addAccount: () => {},
            editAccount: () => {},
            deleteAccount: () => {},
            setPassword: () => {},
        };

        const wrapper = shallow(<AccountPage {...props} />);
        expect(wrapper.children()).toHaveLength(2);
    });
});
