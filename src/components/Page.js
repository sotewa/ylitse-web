import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

const styles = theme => ({
    root: theme.mixins.gutters({
        marginTop: theme.spacing.unit * 4,
        marginLeft: '16%',
        marginRight: '16%',
        paddingTop: 24,
        paddingBottom: 24,
        overflowX: 'auto',
    }),
});

const Page = ({ header, children, classes }) => (
    <Paper className={classes.root}>
        {header &&
            <Typography variant="headline" component="h3">
                {header}
            </Typography>
        }
        {children}
    </Paper>
);

Page.propTypes = {
    header: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element,
        PropTypes.string,
    ]).isRequired,
    classes: PropTypes.shape({ root: PropTypes.string }).isRequired,
};

Page.defaultProps = {
    header: '',
};

export default withStyles(styles)(Page);
