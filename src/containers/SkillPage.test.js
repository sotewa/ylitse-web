import React from 'react';
import { shallow } from 'enzyme';

import { SkillPage } from './SkillPage';

describe('SkillPage', () => {
    test('renders correctly', () => {
        const props = {
            skills: [{ id: '0', name: 'Test skill' }],
            isFetching: false,
            fetchSkills: () => {},
            createSkill: () => {},
            deleteSkill: () => {},
        };
        const wrapper = shallow(<SkillPage {...props} />);

        expect(wrapper.children()).toHaveLength(2);
    });
});
