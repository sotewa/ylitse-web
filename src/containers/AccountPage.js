import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
    fetchAccounts, addAccount, editAccount, deleteAccount, setPassword,
} from '../actions';
import Loading from '../components/Loading';
import Page from '../components/Page';
import AccountToolbar from '../components/AccountToolbar';
import AccountList from '../components/AccountList';
import ConfirmationDialog from '../components/ConfirmationDialog';

export class AccountPage extends Component {
    static propTypes = {
        accounts: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            username: PropTypes.string,
        })).isRequired,
        isFetching: PropTypes.bool.isRequired,
        fetchAccounts: PropTypes.func.isRequired,
        addAccount: PropTypes.func.isRequired,
        editAccount: PropTypes.func.isRequired,
        deleteAccount: PropTypes.func.isRequired,
        setPassword: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            selectedAccount: undefined,
            deletingAccount: false,
        };
    }

    componentDidMount() {
        this.props.fetchAccounts();
    }

    setPassword = account => () => {
        this.props.setPassword(account);
    }

    openConfirmation = selectedAccount => () => {
        this.setState({ selectedAccount, deletingAccount: true });
    }

    closeConfirmation = () => {
        this.setState({ selectedAccount: undefined, deletingAccount: false });
    }

    editAccount = account => () => {
        this.props.editAccount(account);
    }

    deleteAccount = account => () => {
        this.props.deleteAccount(account);
        this.closeConfirmation();
    }

    render() {
        const { accounts, isFetching } = this.props;
        const { selectedAccount, deletingAccount } = this.state;
        const accountName = selectedAccount &&
                            `${selectedAccount.username}'s account`;

        return (
            <Page header="User accounts">
                <AccountToolbar onAddClick={this.props.addAccount} />
                {isFetching &&
                    <Loading />
                }
                {!isFetching &&
                    <AccountList
                        accounts={accounts}
                        onEdit={this.editAccount}
                        onDelete={this.openConfirmation}
                        onPassChange={this.setPassword}
                    />
                }
                {selectedAccount &&
                    <ConfirmationDialog
                        open={deletingAccount}
                        label={`Really delete ${accountName}?`}
                        okLabel="Delete"
                        onOkClick={this.deleteAccount(selectedAccount)}
                        onClose={this.closeConfirmation}
                    />
                }
            </Page>
        );
    }
}

function mapStateToProps({ accounts }) {
    return {
        accounts: accounts.items,
        isFetching: accounts.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchAccounts, addAccount, editAccount, deleteAccount, setPassword,
})(AccountPage);
