import {
    capitalize, camelify, snakify, updateKeys, updateWith, sortBy,
} from './utils';

describe('capitalize', () => {
    test('works with lower case', () => {
        expect(capitalize('test')).toEqual('Test');
    });

    test('works with empty string', () => {
        expect(capitalize('')).toEqual('');
    });
});

describe('camelify', () => {
    test('works with snake case', () => {
        expect(camelify('test_snake_case')).toEqual('testSnakeCase');
    });

    test('works with empty string', () => {
        expect(camelify('')).toEqual('');
    });
});

describe('snakify', () => {
    test('works with camel case', () => {
        expect(snakify('testCamelCase')).toEqual('test_camel_case');
    });

    test('works with empty string', () => {
        expect(snakify('')).toEqual('');
    });
});

describe('updateKeys', () => {
    test('works with simple object', () => {
        const obj = { key1: 'value1', key2: 'value2' };
        expect(updateKeys(obj, k => k)).toEqual(obj);
    });

    test('works with empty object', () => {
        const obj = {};
        expect(updateKeys(obj, k => k)).toEqual(obj);
    });
});

describe('updateWith', () => {
    test('works with simple list', () => {
        const objs = [{ id: '0', name: 'A' }, { id: '1', name: 'B' }];
        expect(updateWith({ id: '1', name: 'C' }, objs)).toEqual([
            { id: '0', name: 'A' },
            { id: '1', name: 'C' },
        ]);
    });

    test('works with empty list', () => {
        const obj = { id: '0', name: 'A' };
        expect(updateWith(obj, [])).toEqual([obj]);
    });
});

describe('sortBy', () => {
    test('works with simple list', () => {
        const objs = [{ name: 'B' }, { name: 'A' }];
        expect(sortBy('name', objs)).toEqual([
            { name: 'A' },
            { name: 'B' },
        ]);
    });

    test('works with empty list', () => {
        const objs = [];
        expect(sortBy('name', objs)).toEqual(objs);
    });

    test('works with list of empty objects', () => {
        const objs = [{}, {}];
        expect(sortBy('name', objs)).toEqual(objs);
    });
});
