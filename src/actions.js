import { updateKeys, camelify, snakify } from './utils';
import { DialogTypes } from './containers/DialogWindow';

export const FeedbackActionTypes = {
    ENQUEUE: 'FEEDBACK_ENQUEUE',
    DEQUEUE: 'FEEDBACK_DEQUEUE',
};

export const VersionActionTypes = {
    REQUEST: 'VERSION_REQUEST',
    SUCCESS: 'VERSION_SUCCESS',
    FAILURE: 'VERSION_FAILURE',
};

export const UserActionTypes = {
    REQUEST: 'USER_REQUEST',
    SUCCESS: 'USER_SUCCESS',
    FAILURE: 'USER_FAILURE',
};

export const LogoutActionTypes = {
    REQUEST: 'LOGOUT_REQUEST',
    SUCCESS: 'LOGOUT_SUCCESS',
    FAILURE: 'LOGOUT_FAILURE',
};

export const SkillsActionTypes = {
    FETCH_REQUEST: 'SKILLS_FETCH_REQUEST',
    FETCH_SUCCESS: 'SKILLS_FETCH_SUCCESS',
    FETCH_FAILURE: 'SKILLS_FETCH_FAILURE',
    CREATE_REQUEST: 'SKILLS_CREATE_REQUEST',
    CREATE_SUCCESS: 'SKILLS_CREATE_SUCCESS',
    CREATE_FAILURE: 'SKILLS_CREATE_FAILURE',
    DELETE_REQUEST: 'SKILLS_DELETE_REQUEST',
    DELETE_SUCCESS: 'SKILLS_DELETE_SUCCESS',
    DELETE_FAILURE: 'SKILLS_DELETE_FAILURE',
};

export const AccountsActionTypes = {
    FETCH_REQUEST: 'ACCOUNTS_FETCH_REQUEST',
    FETCH_SUCCESS: 'ACCOUNTS_FETCH_SUCCESS',
    FETCH_FAILURE: 'ACCOUNTS_FETCH_FAILURE',
    CREATE_REQUEST: 'ACCOUNTS_CREATE_REQUEST',
    CREATE_SUCCESS: 'ACCOUNTS_CREATE_SUCCESS',
    CREATE_FAILURE: 'ACCOUNTS_CREATE_FAILURE',
    UPDATE_REQUEST: 'ACCOUNTS_UPDATE_REQUEST',
    UPDATE_SUCCESS: 'ACCOUNTS_UPDATE_SUCCESS',
    UPDATE_FAILURE: 'ACCOUNTS_UPDATE_FAILURE',
    DELETE_REQUEST: 'ACCOUNTS_DELETE_REQUEST',
    DELETE_SUCCESS: 'ACCOUNTS_DELETE_SUCCESS',
    DELETE_FAILURE: 'ACCOUNTS_DELETE_FAILURE',
};

export const DialogActionTypes = {
    OPEN: 'DIALOG_OPEN',
    CLOSE: 'DIALOG_CLOSE',
};

export function enqueueFeedback(message) {
    return {
        type: FeedbackActionTypes.ENQUEUE,
        message,
    };
}

export function dequeueFeedback() {
    return {
        type: FeedbackActionTypes.DEQUEUE,
    };
}

async function request(endpoint, method, headers, body) {
    const resp = await fetch(endpoint, {
        method,
        headers,
        body,
        redirect: 'follow',
        credentials: 'include',
    });
    const contentType = resp.headers.get('Content-Type');
    const isJson = contentType && contentType.includes('application/json');

    if (resp.redirected) {
        window.location.replace(resp.url);

        // Don't really show the message
        throw new Error('');
    }

    if (!resp.ok || resp.status >= 400) {
        if (isJson) {
            const json = await resp.json();

            throw new Error(json.message || resp.statusText);
        }

        throw new Error(resp.statusText);
    }

    return isJson ? resp.json() : null;
}

function GET(endpoint, requestAction, failureAction, receiveAction) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            const data = await request(endpoint, 'GET');

            if (data && 'resources' in data) {
                dispatch(receiveAction(
                    data.resources.map(r => updateKeys(r, camelify)),
                ));
            } else {
                dispatch(receiveAction(updateKeys(data, camelify)));
            }
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

function write(
    endpoint, object, requestAction, failureAction, receiveAction,
    receiveMessage, method,
) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            const headers = { 'Content-Type': 'application/json' };
            const body = JSON.stringify(updateKeys(object, snakify));
            const data = await request(endpoint, method, headers, body);

            await dispatch(receiveAction(updateKeys(data, camelify)));
            dispatch(enqueueFeedback(receiveMessage));
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

function POST(...args) {
    return write(...args, 'POST');
}

function PUT(...args) {
    return write(...args, 'PUT');
}

function DELETE(
    endpoint, requestAction, failureAction, receiveAction, receiveMessage,
) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            await request(endpoint, 'DELETE');
            await dispatch(receiveAction());
            dispatch(enqueueFeedback(receiveMessage));
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

export function fetchVersion() {
    return (dispatch, getState) => {
        if (getState().version.api || getState().version.isFetching) {
            return null;
        }

        return dispatch(GET(
            '/api/version',
            () => ({ type: VersionActionTypes.REQUEST }),
            () => ({ type: VersionActionTypes.FAILURE }),
            version => ({ type: VersionActionTypes.SUCCESS, version }),
        ));
    };
}

export function fetchUser() {
    return (dispatch, getState) => {
        if (getState().user.username || getState().user.isFetching) {
            return null;
        }

        return dispatch(GET(
            '/api/user',
            () => ({ type: UserActionTypes.REQUEST }),
            () => ({ type: UserActionTypes.FAILURE }),
            user => ({ type: UserActionTypes.SUCCESS, user }),
        ));
    };
}

function reload() {
    window.location.replace('/');

    return { type: '' };
}

export function logout() {
    return (dispatch, getState) => {
        if (getState().logout.inProgress) {
            return null;
        }

        return dispatch(GET(
            '/api/logout',
            () => ({ type: LogoutActionTypes.REQUEST }),
            () => ({ type: LogoutActionTypes.FAILURE }),
            reload,
        ));
    };
}

export function fetchSkills() {
    return (dispatch, getState) => {
        if (getState().skills.isFetching) {
            return null;
        }

        return dispatch(GET(
            '/api/skills',
            () => ({ type: SkillsActionTypes.FETCH_REQUEST }),
            () => ({ type: SkillsActionTypes.FETCH_FAILURE }),
            skills => ({ type: SkillsActionTypes.FETCH_SUCCESS, skills }),
        ));
    };
}

export function createSkill(skillName) {
    return (dispatch, getState) => {
        if (getState().skills.isCreating) {
            return null;
        }

        return dispatch(POST(
            '/api/skills',
            { name: skillName },
            () => ({ type: SkillsActionTypes.CREATE_REQUEST }),
            () => ({ type: SkillsActionTypes.CREATE_FAILURE }),
            skill => ({ type: SkillsActionTypes.CREATE_SUCCESS, skill }),
            `${skillName} skill created`,
        ));
    };
}

export function deleteSkill(skill) {
    return (dispatch, getState) => {
        if (getState().skills.isDeleting) {
            return null;
        }

        const { id, name } = skill;

        return dispatch(DELETE(
            `/api/skills/${id}`,
            () => ({ type: SkillsActionTypes.DELETE_REQUEST }),
            () => ({ type: SkillsActionTypes.DELETE_FAILURE }),
            () => ({ type: SkillsActionTypes.DELETE_SUCCESS, id }),
            `${name} skill deleted`,
        ));
    };
}

export function fetchAccounts() {
    return (dispatch, getState) => {
        if (getState().accounts.isFetching) {
            return null;
        }

        return dispatch(GET(
            '/api/accounts',
            () => ({ type: AccountsActionTypes.FETCH_REQUEST }),
            () => ({ type: AccountsActionTypes.FETCH_FAILURE }),
            accounts => ({ type: AccountsActionTypes.FETCH_SUCCESS, accounts }),
        ));
    };
}

export function createAccount(newAccount) {
    return (dispatch, getState) => {
        if (getState().accounts.isCreating) {
            return null;
        }

        return dispatch(POST(
            '/api/accounts',
            newAccount,
            () => ({ type: AccountsActionTypes.CREATE_REQUEST }),
            () => ({ type: AccountsActionTypes.CREATE_FAILURE }),
            account => ({ type: AccountsActionTypes.CREATE_SUCCESS, account }),
            `${newAccount.username}'s account created`,
        ));
    };
}

export function updateAccount(account) {
    return (dispatch, getState) => {
        if (getState().accounts.isUpdating) {
            return null;
        }

        const { id, username } = account;

        return dispatch(PUT(
            `/api/accounts/${id}`,
            account,
            () => ({ type: AccountsActionTypes.UPDATE_REQUEST }),
            () => ({ type: AccountsActionTypes.UPDATE_FAILURE }),
            a => ({ type: AccountsActionTypes.UPDATE_SUCCESS, account: a }),
            `${username}'s account updated`,
        ));
    };
}

export function deleteAccount(account) {
    return (dispatch, getState) => {
        if (getState().accounts.isDeleting) {
            return null;
        }

        const { id, username } = account;

        return dispatch(DELETE(
            `/api/accounts/${id}`,
            () => ({ type: AccountsActionTypes.DELETE_REQUEST }),
            () => ({ type: AccountsActionTypes.DELETE_FAILURE }),
            () => ({ type: AccountsActionTypes.DELETE_SUCCESS, id }),
            `${username}'s account deleted`,
        ));
    };
}

export function addAccount() {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.ADD_ACCOUNT,
        dialogProps: {},
    };
}

export function editAccount(account) {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.EDIT_ACCOUNT,
        dialogProps: { account },
    };
}

export function closeDialog() {
    return {
        type: DialogActionTypes.CLOSE,
    };
}

export function setPassword(account) {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.SET_PASSWORD,
        dialogProps: { account },
    };
}
