import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog, {
    DialogContent, DialogContentText, DialogTitle,
} from 'material-ui/Dialog';
import Snackbar from 'material-ui/Snackbar';

import { dequeueFeedback } from '../actions';

class Feedback extends Component {
    static propTypes = {
        critical: PropTypes.bool,
        message: PropTypes.string.isRequired,
        multiple: PropTypes.bool.isRequired,
        dequeueFeedback: PropTypes.func.isRequired,
    }

    static defaultProps = {
        critical: false,
    }

    constructor(props) {
        super(props);

        this.message = '';
    }

    render() {
        const { critical, message, multiple } = this.props;

        if (critical) {
            return (
                <Dialog open={Boolean(message)}>
                    <DialogTitle>Error</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {message}
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            );
        }

        if (message) {
            // Copy message to be shown during closing animation
            // when original message is cleared
            this.message = message;
        }

        return (
            <Snackbar
                key={message}
                open={Boolean(message)}
                message={this.message}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                autoHideDuration={2000}
                action={
                    <Button
                        color="secondary"
                        size="small"
                        onClick={this.props.dequeueFeedback}
                    >
                        {multiple ? 'Next' : 'Close'}
                    </Button>
                }
                onClose={this.props.dequeueFeedback}
            />
        );
    }
}

function mapStateToProps({ feedback }) {
    return {
        message: feedback.current,
        multiple: feedback.messages.length > 0,
    };
}

export default connect(mapStateToProps, { dequeueFeedback })(Feedback);
