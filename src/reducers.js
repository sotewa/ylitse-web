import { combineReducers } from 'redux';

import { sortBy, updateWith } from './utils';
import {
    FeedbackActionTypes, DialogActionTypes, VersionActionTypes, UserActionTypes,
    LogoutActionTypes, SkillsActionTypes, AccountsActionTypes,
} from './actions';

export function feedback(state = { messages: [], current: '' }, action) {
    switch (action.type) {
        case FeedbackActionTypes.ENQUEUE:
            // Show the first message right away
            if (!state.current) {
                return { messages: [], current: action.message };
            }

            // Don't show the current message again
            if (action.message === state.current) {
                return state;
            }

            return {
                messages: [...state.messages, action.message],
                current: state.current,
            };
        case FeedbackActionTypes.DEQUEUE:
            // No more messages to show
            if (state.messages.length === 0) {
                return { messages: [], current: '' };
            }

            return {
                messages: state.messages.slice(1),
                current: state.messages[0],
            };
        default:
            return state;
    }
}

const dialogInitialState = { type: '', props: {} };

export function dialog(state = dialogInitialState, action) {
    switch (action.type) {
        case DialogActionTypes.OPEN:
            return {
                type: action.dialogType,
                props: action.dialogProps,
            };
        case DialogActionTypes.CLOSE:
            return dialogInitialState;
        default:
            return state;
    }
}

export function version(state = { api: '', isFetching: false }, action) {
    switch (action.type) {
        case VersionActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case VersionActionTypes.SUCCESS:
            return {
                ...state,
                api: action.version.api,
                isFetching: false,
            };
        case VersionActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

const userInitialState = { role: '', username: '', isFetching: false };

export function user(state = userInitialState, action) {
    switch (action.type) {
        case UserActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case UserActionTypes.SUCCESS:
            return {
                ...state,
                role: action.user.role,
                username: action.user.username,
                isFetching: false,
            };
        case UserActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

export function logout(state = { inProgress: false }, action) {
    switch (action.type) {
        case LogoutActionTypes.REQUEST:
            return { inProgress: true };
        default:
            return state;
    }
}

const skillsInitialState = {
    items: [],
    isFetching: false,
    isCreating: false,
    isDeleting: false,
};

export function skills(state = skillsInitialState, action) {
    switch (action.type) {
        case SkillsActionTypes.FETCH_REQUEST:
            return { ...state, isFetching: true };
        case SkillsActionTypes.FETCH_SUCCESS:
            return {
                ...state,
                items: sortBy('name', action.skills),
                isFetching: false,
            };
        case SkillsActionTypes.FETCH_FAILURE:
            return { ...state, isFetching: false };
        case SkillsActionTypes.CREATE_REQUEST:
            return { ...state, isCreating: true };
        case SkillsActionTypes.CREATE_SUCCESS:
            return {
                ...state,
                items: sortBy('name', [action.skill, ...state.items]),
                isCreating: false,
            };
        case SkillsActionTypes.CREATE_FAILURE:
            return { ...state, isCreating: false };
        case SkillsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case SkillsActionTypes.DELETE_SUCCESS:
            return {
                ...state,
                items: state.items.filter(s => s.id !== action.id),
                isDeleting: false,
            };
        case SkillsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        default:
            return state;
    }
}

const accountsInitialState = {
    items: [],
    isFetching: false,
    isCreating: false,
    isUpdating: false,
    isDeleting: false,
};

export function accounts(state = accountsInitialState, action) {
    switch (action.type) {
        case AccountsActionTypes.FETCH_REQUEST:
            return { ...state, isFetching: true };
        case AccountsActionTypes.FETCH_SUCCESS:
            return {
                ...state,
                items: sortBy('username', action.accounts),
                isFetching: false,
            };
        case AccountsActionTypes.FETCH_FAILURE:
            return { ...state, isFetching: false };
        case AccountsActionTypes.CREATE_REQUEST:
            return { ...state, isCreating: true };
        case AccountsActionTypes.CREATE_SUCCESS:
            return {
                ...state,
                items: sortBy('username', [action.account, ...state.items]),
                isCreating: false,
            };
        case AccountsActionTypes.CREATE_FAILURE:
            return { ...state, isCreating: false };
        case AccountsActionTypes.UPDATE_REQUEST:
            return { ...state, isUpdating: true };
        case AccountsActionTypes.UPDATE_SUCCESS:
            return {
                ...state,
                items: updateWith(action.account, state.items),
                isUpdating: false,
            };
        case AccountsActionTypes.UPDATE_FAILURE:
            return { ...state, isUpdating: false };
        case AccountsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case AccountsActionTypes.DELETE_SUCCESS:
            return {
                ...state,
                items: state.items.filter(s => s.id !== action.id),
                isDeleting: false,
            };
        case AccountsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    feedback, dialog, version, user, logout, skills, accounts,
});

export default rootReducer;
