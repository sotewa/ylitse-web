import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { render } from 'react-dom';
import { MuiThemeProvider } from 'material-ui/styles';
import 'whatwg-fetch';
import 'typeface-roboto/index.css';

import theme from './theme';
import rootReducer from './reducers';
// eslint-disable-next-line import/no-named-as-default
import RootContainer from './containers/Root';

const middleware = [thunk];

if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger());
}

const store = createStore(
    rootReducer,
    applyMiddleware(...middleware),
);

if (module.hot) {
    module.hot.accept('./reducers', () => {
        store.replaceReducer(rootReducer);
    });
}

const root = (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <RootContainer />
        </MuiThemeProvider>
    </Provider>
);

render(root, document.getElementById('root'));
