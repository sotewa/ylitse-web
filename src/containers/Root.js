import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import { UserRoles } from '../constants';
import { fetchUser } from '../actions';
import DialogWindow from './DialogWindow';
import Feedback from './Feedback';
// eslint-disable-next-line import/no-named-as-default
import Navigation from './Navigation';
// eslint-disable-next-line import/no-named-as-default
import AccountPage from './AccountPage';
// eslint-disable-next-line import/no-named-as-default
import SkillPage from './SkillPage';
import MissingPage from '../components/MissingPage';

export class Root extends Component {
    static propTypes = {
        username: PropTypes.string.isRequired,
        role: PropTypes.string.isRequired,
        fetchUser: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.fetchUser();
    }

    render() {
        const { role, username } = this.props;
        const rootPath = role === UserRoles.ADMIN ? '/accounts' : '/dashboard';

        if (!username) {
            return <Feedback critical />;
        }

        return (
            <BrowserRouter>
                <main>
                    <Navigation />
                    <Switch>
                        <Route path="/" exact><Redirect to={rootPath} /></Route>
                        {role === UserRoles.ADMIN &&
                            <Route
                                path="/accounts"
                                exact
                                component={AccountPage}
                            />
                        }
                        {role === UserRoles.ADMIN &&
                            <Route
                                path="/skills"
                                exact
                                component={SkillPage}
                            />
                        }
                        <Route component={MissingPage} />
                    </Switch>
                    <DialogWindow />
                    <Feedback />
                </main>
            </BrowserRouter>
        );
    }
}

function mapStateToProps({ user }) {
    return {
        role: user.role,
        username: user.username,
    };
}

export default connect(mapStateToProps, { fetchUser })(Root);
